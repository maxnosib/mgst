//CODE GENERATED AUTOMATICALLY
//THIS FILE SHOULD NOT BE EDITED BY HAND
package example

import (
	"database/sql"
	"fmt"
)

//go:generate mds sts example.go

// GetAllField struct to string.
func (in *GenCheck) GetAllField() (string, string) {
	return "id,FieldS,FieldSP,FieldI,FieldIP,FieldB,FieldBP,FieldM,FieldSl", "$1,$2,$3,$4,$5,$6,$7,$8,$9"
}

// GetInsField struct to string.
func (in *GenCheck) GetInsField() (string, string) {
	return "FieldS,FieldSP,FieldI,FieldIP,FieldB,FieldBP,FieldM,FieldSl", "$1,$2,$3,$4,$5,$6,$7,$8"
}

// Validate struct.
func (in *GenCheck) Validate() error {

	if in.FieldI == 0 {
		return fmt.Errorf("FieldI not be empty")
	}

	if len(in.FieldM) == 0 {
		return fmt.Errorf("FieldM not be empty")
	}

	if in.FieldS == "" {
		return fmt.Errorf("FieldS not be empty")
	}

	if len(in.FieldSl) == 0 {
		return fmt.Errorf("FieldSl not be empty")
	}

	return nil
}

// RowsToStruct raw data sql to struct type.
func RowsToStructGenCheck(rows *sql.Rows) ([]GenCheck, error) {
	var result []GenCheck
	for rows.Next() {
		var row GenCheck
		err := rows.Scan(
			&row.ID,
			&row.FieldS,
			&row.FieldSP,
			&row.FieldI,
			&row.FieldIP,
			&row.FieldB,
			&row.FieldBP,
			&row.FieldM,
			&row.FieldSl,
		)
		if err != nil {
			return nil, err
		}

		result = append(result, row)
	}

	return result, nil
}

package example

//sts: gen
// GenCheck struct from check generate.
type GenCheck struct {
	ID      string           `sts:"id"`
	FieldS  string           `sts:"FieldS"`
	FieldSP *string          `sts:"FieldSP"`
	FieldI  int              `sts:"FieldI"`
	FieldIP *int             `sts:"FieldIP"`
	FieldB  bool             `sts:"FieldB"`
	FieldBP *bool            `sts:"FieldBP"`
	FieldM  map[int]struct{} `sts:"FieldM"`
	FieldSl []struct{}       `sts:"FieldSl"`
}

package stencil

// шаблон шапки сгенерируемого кода.
func GetTemplatePackage() string {
	return `//CODE GENERATED AUTOMATICALLY
//THIS FILE SHOULD NOT BE EDITED BY HAND
package {{.Package}}

import (
	"fmt"
	"database/sql"
)

//go:generate mds {{.Tag}} {{.File}}

`
}

// шаблон для функции генерации sql запроса.
func GetTemplateFromAllGetField() string {
	return `// GetAllField struct to string.
func (in *{{.StructName}}) GetAllField() (string, string) {
	return "{{.FieldsStr}}", "{{.QuestionStr}}"
}

`
}

// шаблон для функции генерации sql запроса.
func GetTemplateFromInsGetField() string {
	return `// GetInsField struct to string.
func (in *{{.StructName}}) GetInsField() (string, string) {
	return "{{.FieldsStr}}", "{{.QuestionStr}}"
}

`
}

// шаблон для функции валидации.
func GetTemplateFromValidate() string {
	return `// Validate struct.
func (in *{{.StructName}}) Validate() error {
	{{range $index, $element := .ListField}}
	{{ if and (eq $element "string")}}
	if in.{{$index}} == "" {
		return fmt.Errorf("{{$index}} not be empty")
	}
	{{end}}
	{{ if and (eq $element "int")}}
	if in.{{$index}} == 0 {
		return fmt.Errorf("{{$index}} not be empty")
	}
	{{end}}
	{{ if and (eq $element "map")}}
	if len(in.{{$index}}) == 0 {
		return fmt.Errorf("{{$index}} not be empty")
	}
	{{end}}
	{{ if and (eq $element "slice")}}
	if len(in.{{$index}}) == 0 {
		return fmt.Errorf("{{$index}} not be empty")
	}
	{{end}}
	{{end}}
	return nil
}

`
}

func GetTemplateScanRows() string {
	return `
// RowsToStruct raw data sql to struct type.
func RowsToStruct{{.StructName}}(rows *sql.Rows)([]{{.StructName}}, error){
	var result []{{.StructName}}
	for rows.Next() {
		var row {{.StructName}}
		err := rows.Scan({{range $index, $element := .ListField}}
			&row.{{$element}},{{end}}
		)
		if err != nil {
			return nil, err
		}

		result = append(result, row)
	}

	return result, rows.Err()
}	

`
}

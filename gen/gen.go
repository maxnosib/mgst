package gen

import (
	"go/ast"
	"go/types"
	"io"
	"log"
	"os"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"text/template"

	"gitlab.com/maxnosib/mgst/stencil"
)

// DataGenCode struct from generate code.
type DataGenCode struct {
	Tag             string
	Package         string
	File            string
	TemplatePackage string
	GetAllFieldFunc GetAllFieldFunc
	GetInsFieldFunc GetInsFieldFunc
	ValidateFunc    ValidateFunc
	ScanRowsFunc    ScanRowsFunc
}

// GetAllFieldFunc struct from generate func GetAllField.
type GetAllFieldFunc struct {
	StructName  string
	Template    string
	FieldsStr   string
	QuestionStr string
}

// GetInsFieldFunc struct from generate func GetInsField.
type GetInsFieldFunc struct {
	StructName  string
	Template    string
	FieldsStr   string
	QuestionStr string
}

// ValidateFunc struct from generate func ValidateFunc.
type ValidateFunc struct {
	StructName string
	Template   string
	ListField  map[string]string // map[field_name]type
}

// ScanRowsFunc struct from generate func getTemplateScanRows.
type ScanRowsFunc struct {
	StructName string
	Template   string
	ListField  []string
}

// заполняем статические поля.
func (dgc *DataGenCode) writeStaticData(
	tag string,
	file string,
	packName string,
	structName string,
) {
	dgc.Tag = tag
	dgc.File = file
	dgc.Package = packName
	dgc.TemplatePackage = stencil.GetTemplatePackage()

	dgc.GetAllFieldFunc.StructName = structName
	dgc.GetAllFieldFunc.Template = stencil.GetTemplateFromAllGetField()

	dgc.GetInsFieldFunc.StructName = structName
	dgc.GetInsFieldFunc.Template = stencil.GetTemplateFromInsGetField()

	dgc.ValidateFunc.StructName = structName
	dgc.ValidateFunc.Template = stencil.GetTemplateFromValidate()
	dgc.ValidateFunc.ListField = make(map[string]string)

	dgc.ScanRowsFunc.StructName = structName
	dgc.ScanRowsFunc.Template = stencil.GetTemplateScanRows()
	dgc.ScanRowsFunc.ListField = make([]string, 0)
}

var regMap = regexp.MustCompile("^map.+")

var regSlice = regexp.MustCompile(`^\[].+`)

// CreateFile создаем файл.
func CreateFile(name string) *os.File {
	out, err := os.Create(name)
	if err != nil {
		log.Fatal(err)
	}

	return out
}

// ParseCode парсим ast дерево.
func ParseCode(node *ast.File, out io.Writer, tagName, fileName string) {
	var res DataGenCode
	for _, f := range node.Decls {
		// получаем декларации из ast
		g, ok := f.(*ast.GenDecl)
		if !ok || g.Doc == nil {
			continue
		}

		for _, spec := range g.Specs {
			// проверяем что это базовые ф-ции
			currType, ok := spec.(*ast.TypeSpec)
			if !ok {
				continue
			}

			// проверяем что тип является структурой
			currStruct, ok := currType.Type.(*ast.StructType)
			if !ok {
				continue
			}

			needCodegen := false
			for _, comment := range g.Doc.List {
				// проверяем что структура имеет нужный коммент
				needCodegen = needCodegen || strings.HasPrefix(comment.Text, "//"+tagName+": gen")
			}
			if !needCodegen {
				continue
			}

			// заполняем основные поля структуры
			res.writeStaticData(tagName, fileName, node.Name.Name, currType.Name.Name)

			var allF, allS, insF, insS []string
			countSQL := 1
			idCount := 0

			for _, field := range currStruct.Fields.List {
				if !strings.EqualFold(field.Names[0].Name, "id") {
					if fieldType, ok := typeToString(field.Type); ok {
						res.ValidateFunc.ListField[field.Names[0].Name] = fieldType
					}
				}

				// вычитываем теги
				if field.Tag != nil {
					tag := reflect.StructTag(field.Tag.Value[1 : len(field.Tag.Value)-1])
					if tag.Get(tagName) == "-" {
						continue
					}

					allF = append(allF, tag.Get(tagName))
					allS = append(allS, "$"+strconv.Itoa(countSQL))

					if tag.Get(tagName) != "id" {
						insF = append(insF, tag.Get(tagName))
						insS = append(insS, "$"+strconv.Itoa(countSQL-idCount))
					} else {
						idCount++
					}
					countSQL++

					if len(field.Names) != 0 {
						res.ScanRowsFunc.ListField = append(res.ScanRowsFunc.ListField, field.Names[0].Name)
					}
				}
			}

			res.GetAllFieldFunc.FieldsStr = strings.Join(allF, ",")
			res.GetAllFieldFunc.QuestionStr = strings.Join(allS, ",")

			res.GetInsFieldFunc.FieldsStr = strings.Join(insF, ",")
			res.GetInsFieldFunc.QuestionStr = strings.Join(insS, ",")
		}
	}

	writeCode(out, &res)
}

// записываем данные в шаблом и пишем код в файл.
func writeCode(out io.Writer, data *DataGenCode) {
	pack := template.Must(template.New("package").Parse(data.TemplatePackage))
	if err := pack.Execute(out, data); err != nil {
		log.Printf("template package error: %v", err)
	}

	getAllField := template.Must(template.New("getField").Parse(data.GetAllFieldFunc.Template))
	if err := getAllField.Execute(out, data.GetAllFieldFunc); err != nil {
		log.Printf("template getField error: %v", err)
	}

	getInsField := template.Must(template.New("getField").Parse(data.GetInsFieldFunc.Template))
	if err := getInsField.Execute(out, data.GetInsFieldFunc); err != nil {
		log.Printf("template getField error: %v", err)
	}

	validate := template.Must(template.New("validate").Parse(data.ValidateFunc.Template))
	if err := validate.Execute(out, data.ValidateFunc); err != nil {
		log.Printf("template validate error: %v", err)
	}

	rowsToStruct := template.Must(template.New("rowsToStruct").Parse(data.ScanRowsFunc.Template))
	if err := rowsToStruct.Execute(out, data.ScanRowsFunc); err != nil {
		log.Printf("template rowsToStruct error: %v", err)
	}
}

func typeToString(in ast.Expr) (string, bool) {
	spec := types.ExprString(in)
	switch spec {
	case "*bool", "bool", "*int", "*string":
		return spec, false
	default:
		if regMap.MatchString(spec) {
			spec = "map"
		}
		if regSlice.MatchString(spec) {
			spec = "slice"
		}
	}

	return spec, true
}

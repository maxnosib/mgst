package main

import (
	"go/parser"
	"go/token"
	"log"
	"os"
	"os/exec"
	"path"
	"strings"

	"gitlab.com/maxnosib/mgst/gen"
)

func main() {
	if len(os.Args) != 0 && len(os.Args) >= 3 && os.Args[2] == "{{.File}}" {
		return
	}

	fset := token.NewFileSet()
	// парсим файл из os.Args[1]- название файла
	node, err := parser.ParseFile(fset, os.Args[2], nil, parser.ParseComments)
	if err != nil {
		log.Fatal("error parse file: ", err)
	}

	// создаем имая для сгенерированноего файла
	fileName := strings.TrimSuffix(os.Args[2], ".go") + "_" + os.Args[1] + ".go"

	// отформатируем сгенерированный файл
	// TODO: попробовать форматировать через:
	// /go/sdk/go1.16.2/src/go/format - стандартное форматирование Go по ast.Node
	defer func() {
		er := exec.Command("gofmt", "-w", fileName).Run()
		if er != nil {
			log.Fatalf("format error: %+v", er)
		}
	}()

	// моздаем файл в который будем писать генерируемый код
	fd := gen.CreateFile(fileName)

	gen.ParseCode(node, fd, os.Args[1], getFileName(os.Args[2]))

}

func getFileName(in string) string {
	_, file := path.Split(in)

	return file
}
